CONTEXTO DE LA APLICACIÓN

- Se ejecutan las actividades en un maquina con Sistema Operativo Ubuntu, con Docker y docker-compose instalado.

DESAROLLO DE LA ACTIVIDAD

- Se crea un nuevo directorio local donde se ejecutará el "docker-compose.yaml" para generar la instacia de Jenkins, adicionando tambien un directorio "jenkins_home" para compartir con el contenedor docker, y evitar que se pierda información, en caso de ser necesario crear la instancia nuevamente.
- Usando el archivo "docker-compose.yaml" cargado en el repositorio, se crea y se activa una instancia dockerizada de Jenkins.
- En la nueva instancia de Jenkins, se instalan los plugins por defecto, incluyendo el plugin de Maven con una instalación denominada 'jenkins-maven'.
- Se configuran credenciales en Jenkins para accesar al repositorio de Bitbucket desde el pipeline, las cuales quedaron con el ID: BITBUCKET_PERSONAL_CREDENCIALS.
- Se crea un nuevo proyecto de tipo pipeline con el nombre "maven-test-bitbucket".
- Se configura el nuevo pipeline para que realice checkout del repositorio de Bitbucket y se direccione la ejecución desde el archivo 'Jenkinsfile' que está en raiz del repositorio.
- Se crea repositorio con los branch, "main", "release" y "develop", y se colocan branch "hotfix/jenkinsfile" y "feature/scripts" para simular los branch manejados en Git-flow.
- Se agrega desarrollo básico en Maven y el Jenkinsfile, según el requerimiento:
    - En el stage 'Build', se compila y se crea el .jar.
    - En el stage 'Test', se ejecutan pruebas contra Maven y se colocan post-actions para que "siempre" se guarde el reporte de las pruebas y "unicamente cuando finalicen bien las pruebas" entonces se guarde el .jar en el pipeline de Jenkins para ser consultado.
    - Para el stage 'Deploy', se creó previamente un script "deploy.sh", el cual contiene la simulación del despliegue, copiando el archivo .jar generado por el pipeline hacia una nueva carpeta dentro del workspace de Jenkins. Dentro del stage, se asignan permisos de ejecución para el script y se ejecuta el mismo.

- Para evidencia de lo anterior, agrega al repositorio el archivo "reporte-pipeline.txt" con la compilación exitosa del pipeline descrito.
